document.addEventListener('turbolinks:load', function(){
  var searchInput = $('#posts-search');
  searchInput.on('input', function(){
    if (searchInput.val().length > 2) {
      $.ajax({
        url: '/posts',
        type: 'GET',
        dataType: 'script',
        data: {q: searchInput.val()}
      })
    } else {
      $.ajax({
        url: '/posts',
        type: 'GET',
        dataType: 'script',
        data: {q: ""}
      })
    }
  });
})
